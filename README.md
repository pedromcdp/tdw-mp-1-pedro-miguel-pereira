![Logo](https://lh3.googleusercontent.com/proxy/TAdCCrsTHEjsZSxkg_FbBaSwb88ih1SgDIY5p8ZqcXX4ghbmilUTgI-y_E-fYv8hjZBjx8dwbc1FRkeLyhQcy4i7M-9lllrn8855L2d2CbQF)

# MCTW - MP1 - Pedro Miguel Pereira

Mini projecto 1 da cadeira de Tecnologias e Desenvolvimento Web do Mestrado em Comunicação e Tecnologias Web da Universidade de Aveiro

## Badges

[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://github.com/tterb/atomic-design-ui/blob/master/LICENSEs)
[![pipeline status](https://gitlab.com/pedromcdp/tdw-mp-1-pedro-miguel-pereira/badges/dev/pipeline.svg)](https://gitlab.com/pedromcdp/tdw-mp-1-pedro-miguel-pereira/-/commits/master)
[![coverage report](https://gitlab.com/pedromcdp/tdw-mp-1-pedro-miguel-pereira/badges/dev/coverage.svg)](https://gitlab.com/pedromcdp/tdw-mp-1-pedro-miguel-pereira/-/commits/master)
[![Netlify Status](https://api.netlify.com/api/v1/badges/6f64a5a8-22e3-4b53-919b-5e192e1cc341/deploy-status)](https://app.netlify.com/sites/focused-nightingale-b82162/deploys)
[![GitLab Link](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/pedromcdp/tdw-mp-1-pedro-miguel-pereira.git)
[![BitBucket Link](https://img.shields.io/badge/Bitbucket-330F63?style=for-the-badge&logo=bitbucket&logoColor=white)](https://bitbucket.org/pedromcdp/tdw-mp-1-pedro-miguel-pereira/)

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/pedromcdp/tdw-mp-1-pedro-miguel-pereira.git
```

Go to the project directory

```bash
  cd tdw-mp-1-pedro-miguel-pereira
```

Install dependencies

```bash
  yarn
```

Start the server

```bash
  yarn dev
```

## Running Tests

To run tests, run the following command

```bash
  yarn test
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`CONTENTFUL_SPACE_ID`

`CONTENTFUL_ACCESS_TOKEN`

## Authors

- Pedro Miguel Pereira
